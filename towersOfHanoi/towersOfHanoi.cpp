// towersOfHanoi.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"
#include <stack>
#include <iostream>
#include <iomanip> //  setw
void PrintThreeTower(struct RodsOfTowers *rot);
void MoveDisk(int from, int to, struct RodsOfTowers *RoT);
using namespace std;
void PrintOneTower(stack <int> tower);

struct RodsOfTowers {
	stack<int> tower1;
	stack<int> tower2;
	stack<int> tower3;
};

void hanoi_towers(int quantity, int from, int to, int buf_peg, struct RodsOfTowers *RoT)   //quantity-����� �����, from-��������� ��������� �����(1-3),to-�������� ��������� �����(1-3)
{                                                         //buf_peg - ������������� �������(1-3)
	if (quantity > 0)
	{
		hanoi_towers(quantity - 1, from, buf_peg, to, RoT);
		static int cnt = 1;
		cout << "Number iteration: " << cnt++ << endl;
		cout << from << " -> " << to << endl;
		MoveDisk(from, to, RoT);
		PrintThreeTower(RoT);
		hanoi_towers(quantity - 1, buf_peg, to, from, RoT);
	}
}

int main()
{
	setlocale(LC_ALL, "rus");
	struct RodsOfTowers RoT;
	int start_peg, destination_peg, buffer_peg, plate_quantity;
	cout << "***** Attention! The tower is rotated by 90 degrees *****" << endl;
	cout << "Number start tower: 1" << endl;
	//cin >> start_peg;
	start_peg = 1;
	cout << "Number finish tower: 3" << endl;
	//cin >> destination_peg;
	destination_peg = 3;
	cout << "Number buf tower: 2" << endl;
	//cin >> buffer_peg;
	buffer_peg = 2;
	cout << "Numbers of disks: ";
	cin >> plate_quantity;
	for (int i = plate_quantity; i > 0; i--) {
		RoT.tower1.push(i);
	}
	cout << endl;
	PrintThreeTower(&RoT);
	hanoi_towers(plate_quantity, start_peg, destination_peg, buffer_peg, &RoT);
	system("pause");
	return 0;
}

void PrintThreeTower(struct RodsOfTowers *rot) {
	cout << "First tower: ";
	PrintOneTower(rot->tower1);
	cout << "Second tower: ";
	PrintOneTower(rot->tower2);
	cout << "Third tower: ";
	PrintOneTower(rot->tower3);
	cout << "-------------------------------------------------------------------------------" << endl;
}

void MoveDisk(int from, int to, struct RodsOfTowers *RoT) {
	int tmpTower = 0;
	if (from == 1) {
		//cout << "������ � ������� �����" << endl;
		tmpTower = RoT->tower1.top();
		RoT->tower1.pop();
	}
	else if (from == 2) {
		//cout << "������ �� ������� �����" << endl;
		tmpTower = RoT->tower2.top();
		RoT->tower2.pop();
	}
	else {
		//cout << "������ � �������� �����" << endl;
		tmpTower = RoT->tower3.top();
		RoT->tower3.pop();
	}
	if (to == 1) {
		//cout << "������ �� ������ �����" << endl;
		RoT->tower1.push(tmpTower);
	}
	else if (to == 2) {
		//cout << "������ �� ������ �����" << endl;
		RoT->tower2.push(tmpTower);
	}
	else {
		//cout << "������ �� ������ �����" << endl;
		RoT->tower3.push(tmpTower);
	}
}

void PrintOneTower(stack <int> tower) {
	stack <int> tmpTower;
	while (!tower.empty()) {
		tmpTower.push(tower.top());
		tower.pop();
	}

	while (!tmpTower.empty()) {
		cout << setw(4) << "\t" << tmpTower.top() ;
		tmpTower.pop();
	}
	cout << endl;
}